# analysis_middleware

Note:
 - Volume paths are currently hardcoded, therefor one needs to run 'topology -f setup_analysis_cluster.yaml' before running on a different host.    
 - After creating new compose files like this, the commands for the backends need to be changed manually (for now)

Improvements:
- Name of backends cant handle _
    - E.g.: analysis_backend: 
        - 1 #min
        - 3 #max  
    - _ is converted to /
    - Therefor the link leads to http://localhost:8210/analysis/backend/1/health instead of http://localhost:8210/analysis_backend/1/health
    - Needs to be fixed in HTML creation scripts
- Allow custom port for backend server (inside container)
    - Currently hardcoded to 8013
    - Needs to be changed in compose creation and routing code
- Allow for custom commands for backends
    - Needs to be changed in compose creation
    - E.g.: uvicorn asgi:app --port 5000 --host 0.0.0.0 instead of ["topology-backend" ]
- Fix routing so that it allows urls with /
    - http://localhost:8211/analysisBackend/1/api/health routes to analysisBackend1api/health instead of analysisBackend1/api/health
    - Ideally url would be query parameter: http://localhost:8211/analysisBackend/1?path=api/health
- Fix HTML page links
    - api/analyse/analog_scan link links to http://localhost:8211/analysisBackend/1/analog_scan
    - should be http://localhost:8211/analysisBackend/1?path=api/analyse/analog_scan , if query parameter is implemented
- Option to add arguments/body to backend tasks
    - Currently it is not possible to send any parameters to the backend task, this needs to be added
- Support for different request types, currently only get is supported