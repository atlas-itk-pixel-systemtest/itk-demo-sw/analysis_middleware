import os


def is_docker():
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


if is_docker():
    # print ("Running in Docker")

    broker = "pyamqp://guest:guest@rabbitmq"
    backend = "redis://redis:6379/0"
    # backend="rpc://"

else:
    broker = "pyamqp://guest:guest@localhost"
    backend = "redis://localhost:6379/0"


class CeleryConfig:

    enable_utc = True
    # timezone = 'UTC' # "Europe/Paris"
    result_backend = backend
    broker_url = broker
    broker_connection_retry_on_startup = True
    include = ("topology.server.tasks",)
    worker_send_task_events = True
    task_send_sent_event = True
    task_queues = {
        "analysisBackend1": {"routing_key": " analysisBackend1 "},
        "analysisBackend2": {"routing_key": " analysisBackend2 "},
        "analysisBackend3": {"routing_key": " analysisBackend3 "},
    }
