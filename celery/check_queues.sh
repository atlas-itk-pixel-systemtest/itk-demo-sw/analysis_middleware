#!/bin/bash

response=$(curl -s -o /dev/null -w "%{http_code}"  "http://localhost:7201")

if [ "$response" != "200" ]; then
    echo "Request on http://localhost:7201 failed
"
else
    echo "Request on http://localhost:7201 succeeded
"
fi
response=$(curl -s -o /dev/null -w "%{http_code}"  "http://localhost:7202")

if [ "$response" != "200" ]; then
    echo "Request on http://localhost:7202 failed
"
else
    echo "Request on http://localhost:7202 succeeded
"
fi
response=$(curl -s -o /dev/null -w "%{http_code}"  "http://localhost:7203")

if [ "$response" != "200" ]; then
    echo "Request on http://localhost:7203 failed
"
else
    echo "Request on http://localhost:7203 succeeded
"
fi
